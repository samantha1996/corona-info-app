require 'sqlite3'
require 'sinatra'
require 'json'
require 'digest'


enviroment = ENV['CORONA_APP_ENV'] || "production"

db = SQLite3::Database.open("db.sqlite")

if enviroment == "development"
	db.execute("DROP TABLE IF EXISTS registered_cases")
	db.execute("DROP TABLE IF EXISTS users")
end 

db.execute("CREATE TABLE IF NOT EXISTS users (id integer PRIMARY KEY, username text NOT NULL, password text NOT NULL, unique(username))")
db.execute("CREATE TABLE IF NOT EXISTS registered_cases (id integer PRIMARY KEY, province TEXT, case_situation INTEGER, user_id integer, FOREIGN KEY (user_id) REFERENCES users (id) ON UPDATE CASCADE ON DELETE CASCADE);")

=begin 
def check_in_db(database, table, query_element, keyword)
	query = "SELECT #{query_element} FROM #{table} where #{query_element}='#{keyword}'"
	db = database 
	query_result = db.execute(query)
	if query_result[0][1] == keyword
		return true 
	else 
		return false 
	end 
end 
=end 

enable :sessions 

helpers do
 def login?
  if session[:username].nil?
   return false
  else
   return true
  end
 end

 def username
	 return session[:username]
 end 
end

get '/' do
	erb :index 
end 

get '/signup' do 
	erb :signup
end 

post '/signup' do 
	username = params[:username]
	password = Digest::SHA256.hexdigest(params[:password]) #TODO : Adding a salt to this guy :-) 
	repetition_preventation_query = "SELECT username FROM users WHERE username=?"
	query = "INSERT INTO users (username, password) VALUES(?, ?)"
	if params[:password] == params[:password_confirm]
		db.execute(query, username, password) 
		redirect('/')
	else 
		'passwords didnt match' 
	end 
end 

get '/login' do 
	erb :login
end 

post '/login' do 
	username = params[:username]
	password = Digest::SHA256.hexdigest(params[:password])
	#TODO: Login process.
	begin
		db.execute("SELECT username FROM users where password='#{password}'")
		session[:username] = username
		redirect to('/')
	rescue 
		'Not logged in.'
	end
	
end 

get '/register_case' do
	erb :register_case
end 

post '/register_case' do
	#user_id = db.execute("SELECT id FROM users WHERE username='#{session[:username]}'")
	#user_id = user_id[0][1]
	if session[:username].nil? 
		redirect to('/login')
	else 
		# TODO, make it better 
		db.execute("INSERT INTO registered_cases (province, case_situation, user_id) values (?, ?, ?)", [params[:province], params[:situation], user_id])
		redirect to("/register_case")	
	end 
end

get '/condition/:province' do 
	content_type :json

	province = params[:province]
	alive_counter = 0 
	dead_counter  = 0 

	query = db.execute("SELECT case_situation FROM registered_cases WHERE province=?", province)

	for query_element in query do 
		if query_element[0] == 1
			alive_counter += 1
		else 
			dead_counter += 1
		end 
	end 

	result = {"Province" => province, "stats" => {"infected_cases" => alive_counter, "dead_cases" => dead_counter}}

	result = result.to_json 
end 
